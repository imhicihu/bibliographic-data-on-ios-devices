* Hardware
    - a computer with a stable operating system. In this project we use this hardware:
        - Macbook 15"
        - Ipad Pro
        - Ipad Air 2
        - ipod touch
        
* Software
    * Database
        - [FileMaker](https://www.filemaker.com/): database handling & management
    * Data validators
        - [CSV Validation Tool](https://github.com/digital-preservation/csv-validator)
        - [UTF-8 Validation Tool](https://github.com/digital-preservation/utf8-validator)
    * Code editor
        - [Aquamacs Emacs](http://aquamacs.org/download-release.shtml): Editor for Text, HTML, LaTeX, C++, Java, Python, R, Perl, Ruby, PHP, and more...
        - [Typora](https://typora.io/) (table editing, converting, formatting, exporting to markdown format)
    * Code sharing
        - [Carbon](https://carbon.now.sh/): (Automatization on code screen sharing)
    - GIT workflow
        - [SourceTree](https://www.sourcetreeapp.com/): GIT client
    - Color palette
        - [Flat UI colors](https://flatuicolors.com/)
    - Device simulator
        - [Responsively App](https://github.com/manojVivek/responsively-app): device profiles simulator
    - Screen capture (iOS platform)
        - [Record it! Screen Recorder](https://apps.apple.com/co/app/record-it-screen-recorder/id1245356545): screen recorder on iOS environment for the sake of demonstration

* Online tools
    - [OpenRefine](http://openrefine.org/)
    - [Smash](https://www.fromsmash.com/): unlimited moving/transfer of digital assets
    - [Javascript scripting](https://www.javascripting.com/): source of the best JavaScript libraries, frameworks, and plugins
    - Table converter
        - [Table convert](https://tableconvert.com/): converter to a plethora of formats
    - [Plantuml](http://www.plantuml.com/plantuml/uml/):  Diagram / deployment diagram / critical path
    - Plugins (curated and discriminated by topics)
        - [Awesome design plugins](https://flawlessapp.io/designplugins)
    
* Documentation
     - [Developer certificate](https://developercertificate.org/)
     - *Addendum for Filemaker*: Addendum is an application that lists the functions, script steps, script triggers, error codes and glossary of FileMaker. More data can be found [here](https://apps.apple.com/es/app/addendum-for-filemaker/id1076169380)
* ![screen_capture.png](https://bitbucket.org/repo/nk7jA86/images/3140685935-626x0w.png)

     
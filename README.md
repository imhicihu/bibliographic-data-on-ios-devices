![stability-wip](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-open](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)

# RATIONALE #

* Some efforts are done meanwhile the [Android app](https://bitbucket.org/imhicihu/bibliographical-hybrid-mobile-app) is _in the making_
* This repo is a living document that will grow and adapt over time

### What is this repository for? ###

* Quick summary
    - Taskforce: use [FileMaker Go](https://apps.apple.com/app/filemaker-go-17/id1274628191) technology, we plan to deploy an app that shows up the [Bibliographical database](https://bitbucket.org/imhicihu/terrae-database/src/master/) of the institution.
* Version 1.1

### How do I get set up? ###

* Summary of set up
    - Download [FileMaker Go](https://itunes.apple.com/us/app/filemaker-go-17/id1274628191?mt=8). Then open our [database](https://bitbucket.org/imhicihu/bibliographic-data-on-ios-devices/downloads/BibliotecaIMHICIHU.fmp12) from [`Downloads`](https://bitbucket.org/imhicihu/bibliographic-data-on-ios-devices/downloads/) section and begin to browse and search all the data offered

![search_query.gif](https://bitbucket.org/repo/649o7Ap/images/3335697373-2019-08-06%2014.10.26.gif)

* Configuration
    - There is no need to configure any settings beyond what the [iOS](https://wikipedia.org/wiki/IOS) offer according every iphone/ipad capabilities
    - _Off Topic_: Some previous tech frameworks explored can be found [here](https://bitbucket.org/imhicihu/bibliographic-data-on-ios-devices/issues/1/code-frameworks)
* Dependencies
    - [FileMaker Go](https://itunes.apple.com/us/app/filemaker-go-17/id1274628191?mt=8)
* Database configuration
    - Just download our app and open up with [FileMaker Go](https://apps.apple.com/app/filemaker-go-17/id1274628191)
* How to run tests
    - No need to run tests. This is a closed application. You can't edit any record. Just queries.
* Deployment instructions
    - Just download our app and open up with [FileMaker Go](https://apps.apple.com/app/filemaker-go-17/id1274628191) on your iOS device
    - [Tested](https://bitbucket.org/imhicihu/bibliographic-data-on-ios-devices/src/master/tests.md) on this devices:
        + Filemaker 16 (test passed)
        + Filemaker 17 (test passed)
        + Filemaker 18 (test passed)      

### Related repositories ###

* Some repositories linked with this project:
     - [Bibliographical Searcher - Standalone app](https://bitbucket.org/imhicihu/bibliographical-searcher-stand-alone-app/)
     - [IMHICIHU Digital repository](https://bitbucket.org/digital_repository/imhicihu-digital-repository/)
     - [Terrae database](https://bitbucket.org/imhicihu/terrae-database/src/master/)
     - [WinIsis (migration)](https://bitbucket.org/imhicihu/winisis-migration/src/master/)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/bibliographic-data-on-ios-devices/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/bibliographic-data-on-ios-devices/commits/) section for the current status

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/bibliographic-data-on-ios-devices/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/bibliographic-data-on-ios-devices/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.
## Archiving ##
* [DMG Archiver](https://github.com/coteditor/DMGArchiver): Script for non-AppStore version releasing

## Apple rejections ##
* [Avoiding Common App Rejections](https://developer.apple.com/app-store/review/#common-app-rejections)